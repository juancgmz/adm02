﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesDamage : MonoBehaviour
{
    Renderer rend;
    Color c, originalC;


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        originalC = rend.material.color;
        c = rend.material.color;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Attack")
        {
            StartCoroutine("GetInvulnerable");
        }
    }

    IEnumerator GetInvulnerable()
    {
        c = Color.red;
        rend.material.color = c;
        yield return new WaitForSeconds(0.2f);
        rend.material.color = originalC;
    }
}
