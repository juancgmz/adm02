﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanditHealth : MonoBehaviour
{
    [SerializeField]
    float BanditHp = 40f;
    [SerializeField]
    GameObject Bandit, blood;

    Renderer rend;
    Color c;
    void Start()
    {
        rend = GetComponent<Renderer>();
        c = rend.material.color;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Attack")
        {
            BanditHp = BanditHp - 10;
            if (BanditHp <= 0)
            {
                BanditHp = 0;
                Instantiate(blood, transform.position, Quaternion.identity);
                Destroy(Bandit);
                MyVariableStorage2.BanditKill();
                Bandit.GetComponent<Renderer>().material.color = Color.red;
            }
        }
    }
}
