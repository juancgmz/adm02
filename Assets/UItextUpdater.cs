﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class MyVariableStorage
{
    static GameObject ScoreTxt, MoneyTxt;

    public static float Lives = 20f;
    public static float Score = 0f;
    public static float Money = 10f;

    public static void BatKill()
    {
        Score += 10;
        Money += 1;
        GameObject.FindGameObjectsWithTag("Score#")[0].GetComponent<Text>().text = Score.ToString();
        GameObject.FindGameObjectsWithTag("Money#")[0].GetComponent<Text>().text = Money.ToString();
    }
}
public class UItextUpdater : MonoBehaviour
{
    [SerializeField]
    GameObject LivesText, ScoreText, MoneyText;
    [SerializeField] GameObject GameOverTxt, RestartBtt, QuitBtt;

    private void Start()
    {
        TextChange();
        GameOverTxt.SetActive(false);
        RestartBtt.SetActive(false);
        QuitBtt.SetActive(false);
    }


    public void TextChange()
    {
        LivesText.GetComponent<Text>().text = MyVariableStorage.Lives.ToString();
        ScoreText.GetComponent<Text>().text = MyVariableStorage.Score.ToString();
        MoneyText.GetComponent<Text>().text = MyVariableStorage.Money.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            MyVariableStorage.Lives -= 1;
            if (MyVariableStorage.Score < 500)
                MyVariableStorage.Score = 0;
            else
                MyVariableStorage.Score -= 500;

            if (MyVariableStorage.Lives < 1)
            {
                Time.timeScale = 0f;
                GameOverTxt.SetActive(true);
                RestartBtt.SetActive(true);
                QuitBtt.SetActive(true);
            }
            TextChange();
        }
    }
}
