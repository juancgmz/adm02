﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player : MonoBehaviour
{
    public string FirstName = "John", SecondName = "Bravo", MiddleName = "Sins", LastName = "Depp", FullName = "Jonny";
    public static int speed = 10;
}