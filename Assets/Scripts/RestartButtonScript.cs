﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButtonScript : MonoBehaviour
{
    public void restartScene()
    {
        SceneManager.LoadScene("Game");
        Time.timeScale = 1f;
    }

    public void RestartDarkMines()
    {
        SceneManager.LoadScene("Dark Mine");
        MyVariableStorage2.Kills = 0;
        MyVariableStorage2.Score2 = 0;
        MyVariableStorage2.EnemyCap = 0;
        MyVariableStorage2.AliveEnemies = 0;
        MyVariableStorage2.vidas = 3;
        Time.timeScale = 1f;
    }
}
