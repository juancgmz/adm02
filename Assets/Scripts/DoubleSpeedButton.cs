﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleSpeedButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDoubleSpeed()
    {
        Time.timeScale = 1.5f;
    }
}
