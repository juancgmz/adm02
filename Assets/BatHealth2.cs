﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatHealth2 : MonoBehaviour
{
    [SerializeField]
    float BatHP = 10f;
    [SerializeField]
    GameObject Bat, blood;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Attack")
        {
            BatHP = BatHP - 10;
            if (BatHP <= 0)
            {
                BatHP = 0;
                Instantiate(blood, transform.position, Quaternion.identity);
                Destroy(Bat);
                MyVariableStorage2.BatKill2();
            }
        }
    }

}
