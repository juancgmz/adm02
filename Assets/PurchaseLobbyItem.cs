﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseLobbyItem : MonoBehaviour
{
    [SerializeField] GameObject Plants, Weapons, Dogs, Party, Torches, Misc;
    [SerializeField] GameObject Pb, Wb, Db, Pb2, Tb, Mb;

    private void Start()
    {
        MyVariableStorage2.ComprarAlgo();
    }
    public void PurchasePlants()
    {
        if (MyVariableStorage2.LobbyMoney >= 10)
        {
            Plants.SetActive(true);
            Pb.SetActive(false);
            MyVariableStorage2.LobbyMoney -= 10;
            MyVariableStorage2.ComprarAlgo();
        }
    }

    public void PurchaseWeapons()
    {
        if (MyVariableStorage2.LobbyMoney >= 10)
        {
            Weapons.SetActive(true);
            Wb.SetActive(false);
            MyVariableStorage2.LobbyMoney -= 10;
            MyVariableStorage2.ComprarAlgo();
        }
    }
    public void PurchaseDogs()
    {
        if (MyVariableStorage2.LobbyMoney >= 20)
        {
            Dogs.SetActive(true);
            Db.SetActive(false);
            MyVariableStorage2.LobbyMoney -= 20;
            MyVariableStorage2.ComprarAlgo();
        }
    }

    public void PurchaseParty()
    {
        if (MyVariableStorage2.LobbyMoney >= 8)
        {
            Party.SetActive(true);
            Pb2.SetActive(false);
            MyVariableStorage2.LobbyMoney -= 8;
            MyVariableStorage2.ComprarAlgo();
        }
    }

    public void PurchaseTorches()
    {
        if (MyVariableStorage2.LobbyMoney >= 13)
        {
            Torches.SetActive(true);
            Tb.SetActive(false);
            MyVariableStorage2.LobbyMoney -= 13;
            MyVariableStorage2.ComprarAlgo();
        }
    }

    public void PurchaseMisc()
    {
        if (MyVariableStorage2.LobbyMoney >= 15)
        {
            Misc.SetActive(true);
            Mb.SetActive(false);
            MyVariableStorage2.LobbyMoney -= 15;
            MyVariableStorage2.ComprarAlgo();
        }
    }
}
