﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpawnEnemies : MonoBehaviour
{
    [SerializeField]
    GameObject Bat, SpawnPoint;

    Vector2 whereToSpawn;

    [SerializeField]
    float SpawnRate = 10f, nextSpawn = 0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            if (MyVariableStorage2.EnemyCap < 20)
            {
                nextSpawn = Time.time + SpawnRate;
                whereToSpawn = new Vector2(transform.position.x, transform.position.y);
                Instantiate(Bat, whereToSpawn, Quaternion.identity);
                MyVariableStorage2.EnemyCap++;
                MyVariableStorage2.AliveEnemies++;
                MyVariableStorage2.CambiarTexto();
            }
        }
    }
}
