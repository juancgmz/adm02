﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanditMov : MonoBehaviour
{
    public Animator animator;

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = new Vector3(GameObject.Find("Male Bandit").transform.position.x, GameObject.Find("Male Bandit").transform.position.y, 0.0f);
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Magnitude", movement.magnitude);

    }
}
