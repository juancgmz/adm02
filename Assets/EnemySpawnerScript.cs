﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerScript : MonoBehaviour
{

    [SerializeField]
    GameObject Enemy, SpawnPoint;

    Vector2 whereToSpawn;

    [SerializeField]
    float SpawnRate = 2f, nextSpawn = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextSpawn)
        {
            nextSpawn = Time.time + SpawnRate;
            whereToSpawn = new Vector2(transform.position.x, transform.position.y);
            Instantiate(Enemy, whereToSpawn, Quaternion.identity);
        }
    }
}
