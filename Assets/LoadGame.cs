﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "DarkMines")
        {
            SceneManager.LoadScene("Dark Mine");
            MyVariableStorage2.Kills = 0;
            MyVariableStorage2.Score2 = 0;
            MyVariableStorage2.EnemyCap = 0;
            MyVariableStorage2.AliveEnemies = 0;
            MyVariableStorage2.vidas = 3;
        }
        else if (collision.tag == "TowerDefense")
        {
            SceneManager.LoadScene("Game");
        }
    }
}
