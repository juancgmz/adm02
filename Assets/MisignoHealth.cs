﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisignoHealth : MonoBehaviour
{
    [SerializeField]
    float MisignoHP = 20f;
    [SerializeField]
    GameObject Misigno, blood;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Attack")
        {
            MisignoHP = MisignoHP - 10;
            if (MisignoHP <= 0)
            {
                MisignoHP = 0;
                Instantiate(blood, transform.position, Quaternion.identity);
                Destroy(Misigno);
                MyVariableStorage2.MisignoKill();
                Misigno.GetComponent<Renderer>().material.color = Color.red;
                ExecuteAfterTime(1f);
            }
        }

        IEnumerator ExecuteAfterTime(float time)
        {
            yield return new WaitForSeconds(time);

            Misigno.GetComponent<Renderer>().material.color = Color.clear;
        }
    }
}
