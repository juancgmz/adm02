﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField]
    GameObject Heart1, Heart2, Heart3, player, GameOverText, RestartButton, MainLobbyButton, blood, Head, Hitbox;

    int playerHealth = 3;

    private void Start()
    {
        Heart1 = GameObject.Find("Heart1");
        Heart2 = GameObject.Find("Heart2");
        Heart3 = GameObject.Find("Heart3");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            playerHealth--;

            switch(playerHealth)
            {
                case 2:
                    Heart3.gameObject.SetActive(false);
                    break;
                case 1:
                    Heart2.gameObject.SetActive(false);
                    break;
                case 0:
                    Heart1.gameObject.SetActive(false);
                    break;
            }

            if (playerHealth <= 0)
            {
                Instantiate(blood, transform.position, Quaternion.identity);
                player.SetActive(false);
                GameOverText.SetActive(true);
                RestartButton.SetActive(true);
                MainLobbyButton.SetActive(true);
                Time.timeScale = 0f;
            }
        }
    }
}
