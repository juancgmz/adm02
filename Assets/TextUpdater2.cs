﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class MyVariableStorage2
{
    public static float vidas = 3f;
    public static float Score2 = 0f;
    public static float Kills = 0f;
    public static int EnemyCap = 0;
    public static int AliveEnemies = 0;
    public static float LobbyMoney = 0f;
    public static void BatKill2()
    {
        Score2 += 5;
        Kills += 1;
        LobbyMoney += .05f;
        GameObject.FindGameObjectsWithTag("Score2#")[0].GetComponent<Text>().text = Score2.ToString();
        GameObject.FindGameObjectsWithTag("Kills#")[0].GetComponent<Text>().text = Kills.ToString();
        EnemyCap--;
        AliveEnemies--;
        GameObject.FindGameObjectsWithTag("Enemies#")[0].GetComponent<Text>().text = AliveEnemies.ToString();
    }

    public static void GhastKill()
    {
        Score2 += 85;
        Kills += 1;
        LobbyMoney += .85f;
        GameObject.FindGameObjectsWithTag("Score2#")[0].GetComponent<Text>().text = Score2.ToString();
        GameObject.FindGameObjectsWithTag("Kills#")[0].GetComponent<Text>().text = Kills.ToString();
        EnemyCap--;
        AliveEnemies--;
        GameObject.FindGameObjectsWithTag("Enemies#")[0].GetComponent<Text>().text = AliveEnemies.ToString();
    }
    public static void MisignoKill()
    {
        Score2 += 20;
        Kills += 1;
        LobbyMoney += .2f;
        GameObject.FindGameObjectsWithTag("Score2#")[0].GetComponent<Text>().text = Score2.ToString();
        GameObject.FindGameObjectsWithTag("Kills#")[0].GetComponent<Text>().text = Kills.ToString();
        EnemyCap--;
        AliveEnemies--;
        GameObject.FindGameObjectsWithTag("Enemies#")[0].GetComponent<Text>().text = AliveEnemies.ToString();
    }

    public static void BanditKill()
    {
        Score2 += 50;
        Kills += 1;
        LobbyMoney += .5f;
        GameObject.FindGameObjectsWithTag("Score2#")[0].GetComponent<Text>().text = Score2.ToString();
        GameObject.FindGameObjectsWithTag("Kills#")[0].GetComponent<Text>().text = Kills.ToString();
        EnemyCap--;
        AliveEnemies--;
        GameObject.FindGameObjectsWithTag("Enemies#")[0].GetComponent<Text>().text = AliveEnemies.ToString();
    }

    public static void CambiarTexto()
    {
        GameObject.FindGameObjectsWithTag("Enemies#")[0].GetComponent<Text>().text = AliveEnemies.ToString();
    }

    public static void ComprarAlgo()
    {
        GameObject.FindGameObjectsWithTag("LobbyMoney#")[0].GetComponent<Text>().text = "$ " + LobbyMoney.ToString();
    }
}
public class TextUpdater2 : MonoBehaviour
{
    [SerializeField]
    GameObject KillsTxt, ScoreText, EnemiesTxt, GameOverText, RestartButton, MainMenuButton;

    private void Start()
    {
        TextChange();
    }


    public void TextChange()
    {
        ScoreText.GetComponent<Text>().text = MyVariableStorage2.Score2.ToString();
        KillsTxt.GetComponent<Text>().text = MyVariableStorage2.Kills.ToString();
        EnemiesTxt.GetComponent<Text>().text = MyVariableStorage2.AliveEnemies.ToString();
    }
}
