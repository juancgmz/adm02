﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    Renderer rend;
    Color c, originalC;


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        originalC = rend.material.color;
        c = rend.material.color;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            StartCoroutine("GetInvulnerable");
            StartCoroutine("DamageAnimation");
        }
    }

    IEnumerator GetInvulnerable()
    {
        Physics2D.IgnoreLayerCollision(8, 10, true);
        yield return new WaitForSeconds(2f);
        Physics2D.IgnoreLayerCollision(8, 10, false);
    }

    IEnumerator DamageAnimation()
    {
        c = Color.red;
        rend.material.color = c;
        yield return new WaitForSeconds(.2f);
        rend.material.color = originalC;
    }
}
