﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhastHealth : MonoBehaviour
{
    [SerializeField]
    float GhastHP = 100f;
    [SerializeField]
    GameObject Ghast, blood;

    Renderer rend;
    Color c;
    void Start()
    {
        rend = GetComponent<Renderer>();
        c = rend.material.color;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Attack")
        {
            GhastHP = GhastHP - 10;
            if (GhastHP <= 0)
            {
                GhastHP = 0;
                Instantiate(blood, transform.position, Quaternion.identity);
                Destroy(Ghast);
                MyVariableStorage2.GhastKill();
                Ghast.GetComponent<Renderer>().material.color = Color.red;
            }
        }
    }
}
